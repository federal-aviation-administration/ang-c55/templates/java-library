# java-library

## Usage
This java-library template is implemented through the use of the cookiecutter addon. In order to access this 
template you will need to download cookiecutter. The cookiecutter download and usage instructions can be obtained from 
[download](https://cookiecutter.readthedocs.io/en/latest/installation.html) and [usage](https://cookiecutter.readthedocs.io) 
respectively. If you wish to learn more about cookiecutter and its commands you can do so at 
[more info](https://github.com/cookiecutter/cookiecutter). After the download is complete you can begin to generate a project
from the template using the command `cookiecutter https://gitlab.com/federal-aviation-administration/ang-c55/templates/java-library`. 
This command will prompt you with several inputs, each asking for the names of certain attributes within the project. Once 
these inputs are fufilled a project will generate into your current directory.

### License

#### Creative Commons Zero v1.0 Universal
This is a work created by or on behalf of the United States Government. To the extent that this work is not already in
the public domain by virtue of 17 USC § 105, the FAA waives copyright and neighboring rights in the work worldwide
through the CC0 1.0 Universal Public Domain Dedication (which can be found at https://creativecommons.org/publicdomain/zero/1.0/).
See LICENSE.txt and NOTICE.txt in the root of this project for the full terms of the license.
